package com.example.realmdb.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.realmdb.api.ServerRepository
import com.example.realmdb.modelServer.RealmConfig
import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.modelServer.ServerPort
import com.example.realmdb.models.Member
import com.example.realmdb.models.Server
import com.example.realmdb.models.ServerManager
import com.example.realmdb.utils.SingleLiveData
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmList
import io.realm.RealmResults
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainViewModel(application: Application) : AndroidViewModel(application) {
    var member_result = MutableLiveData<Member>()
    var test_result: String? = null
    var realm : Realm? = null
    private var useRealm = com.example.realmdb.modelServer.Realm()
    var serverDb = com.example.realmdb.modelServer.ServerManager()

    fun addServer(){
        CoroutineScope(Dispatchers.IO).launch {
            var response : Boolean? = ServerRepository().doRequest(getApplication())
            //com.example.realmdb.modelServer.ServerManager().addServer(response)
        }
    }

    fun deleteServer(){
        CoroutineScope(Dispatchers.IO).launch {
            useRealm.deleteAll<ServerList>()
            useRealm.deleteAll<ServerPort>()
        }
    }

    fun update(name: String?, ip: String?){
        CoroutineScope(Dispatchers.IO).launch {
            val server = useRealm.forQuery<ServerList>()?.equalTo("name", name)?.findFirst()
            var data = ServerList()
            server?.apply {
                data.id = this.id
            }
            data.name = name
            data.ip = ip
            //useRealm.copyToRealm(data)
            useRealm.insertOrUpdate(data)
        }
    }

    fun insertServer(name: String?, ip: String?)
    {
        CoroutineScope(Dispatchers.IO).launch {
            val maxId  = useRealm.forQuery<ServerList>()?.max("id")
            var newId: Int = if (maxId == null) 0 else maxId.toInt()+1
            var server = ServerList()
            server.id = newId
            server.name = name
            server.ip = ip
            useRealm.insert(server)
        }
    }

    fun queryAll() : SingleLiveData<RealmResults<ServerList>> {
        var query_result = SingleLiveData<RealmResults<ServerList>>()
        CoroutineScope(Dispatchers.IO).launch {
            var result = useRealm.forQuery<ServerList>()?.findAll()?.sort("name")

            withContext(Dispatchers.Main){
               // var result = useRealm.queryAll<ServerList>()
                query_result.value = result
            }
        }
        return query_result
    }

    fun qureyServer(name: String? , proto: String? , port: Int? = 0) {
        CoroutineScope(Dispatchers.IO).launch {
            var query = useRealm.forQuery<ServerList>()?.sort("name")

            if (name != "") {
                name?.let {
                    query?.equalTo("name", name)?.or()?.contains("name",name)
                    Log.d("Find name", "")
                }
            }

           /* if (proto != "") {
                var resultFirst = query?.findFirst()
                query?.equalTo("port.ip",   resultFirst?.ip)?.findAll()
                        ?.where()?.equalTo("port.protocal",proto)

                Log.d("Find protocal", "")
               // query?.equalTo("port.protocal", proto)
            }
            else if(port != 0){
                port?.let {
                    var resultFirst = query?.findFirst()
                    query?.equalTo("port.ip",   resultFirst?.ip)?.findAll()
                            ?.where()?.equalTo("port.port",port)
                    Log.d("Find port", "")
                }
            }*/

            //find all
            var result: RealmResults<ServerList>? = query?.findAll()
            result?.apply {
                for(item in result){
                    val portlist: RealmList<ServerPort> = item.port
                    var itemprotocal = item.port.filter { pt -> pt.protocal == proto }
                    itemprotocal.forEach{
                        Log.d("Find all protocal", "${it.protocal}")
                    }

                    var itemport = item.port.filter { pt -> pt.port == port }
                    itemport.forEach{
                        Log.d("Find all port", "${it.port}")
                    }

                    /* if (proto != ""){
                         var queryproto: RealmResults<ServerPort> = portlist.where().equalTo("protocal",proto ).findAll()
                         for(p in queryproto)
                             Log.d("Find all protocal", "${p.protocal}")
                     }

                     if (port != 0){
                         var queryport: RealmResults<ServerPort> = portlist.where().equalTo("port", port).findAll()
                         for(p in queryport)
                             Log.d("Find all port", "${p.port}")
                     }*/

                    Log.d("Find all", "$item")
                }
            }
        }
    }

        fun addServertoRealm(name: String, ip: String): SingleLiveData<RealmResults<Server?>> {
            var server_result = SingleLiveData<RealmResults<Server?>>()
            //realm = Realm.getDefaultInstance()
            CoroutineScope(Dispatchers.IO).launch {
                /* var server = Server()
            server.setID("45")
            server.setServerName(name)
            server.setIP(ip)
            ServerManager().addServer(server)*/
                ServerManager().addServerfromCreate(name, ip)
                //readServerfromRealm()
                var result = realm?.where(Server::class.java)?.findAll()
                withContext(Dispatchers.Main) {
                    server_result.value = result
                }
            }
            return server_result
        }

        suspend fun readServerfromRealm() {
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {

                    var result = realm?.where(Server::class.java)?.findAll()
                    //server_result.value = ServerManager().getServer()
                    //server_result.value = result
                }
            }
        }


        fun retrieveMember() {
            CoroutineScope(Dispatchers.IO).launch {
                var member: Member? = null
                // member?.username = "Mata"
                // member?.email = "Mata@gmail.com"

                // Realm.init()
                //realm = Realm.getDefaultInstance()
                //realmConfig = Builder().build()
                //realm = Realm.getInstance(realmConfig)
                /*  realm?.beginTransaction()
            var user = realm?.createObject(Member::class.java)
            user?.firstName = "Lilly"
            user?.studentId =  305
            //realm?.createObject(UserRealm::class.java)
            realm?.commitTransaction()

            try {
                val result: RealmResults<Member> = realm!!.where(Member::class.java).findAll()
                for (member in result) {
                    Log.d("Realm", "Read realm : \nfirstname: ${member.firstName}")
                }
            }catch (e:IOException){
                Log.d("Realm error","${e.message}")
            }
            withContext(Dispatchers.Main){
                member_result.value = user
            }*/
            }
        }

        fun test() {
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {
                    test_result = "Testing..."
                }
            }
        }
    }
