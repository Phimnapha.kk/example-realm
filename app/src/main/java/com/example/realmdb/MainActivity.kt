package com.example.realmdb

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.realmdb.modelServer.Realm
import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.models.Member
import com.example.realmdb.models.Server
import com.example.realmdb.models.ServerManager
import com.example.realmdb.viewmodel.MainViewModel
import io.realm.Realm.init
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener {
    private val mainViewModel: MainViewModel by viewModels()

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_add_item -> onClickAddItem()
            R.id.btn_add_server -> onClickAddServer()
            R.id.btn_update -> onClickUpdateServer()
            R.id.btn_find -> onClickSearchServer()
            R.id.btn_read -> onClickReadServer()
            R.id.btn_delete -> onClickDeleteServer()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init(this)

        btn_add_item.setOnClickListener(this)
        btn_add_server.setOnClickListener(this)
        btn_update.setOnClickListener(this)
        btn_read.setOnClickListener(this)
        btn_find.setOnClickListener(this)
        btn_delete.setOnClickListener(this)
    }

    private fun onClickAddServer(){
        Log.d("Click", "Server")
        mainViewModel.addServer()
    }

    private fun onClickAddItem(){
        Log.d("Click", "Add")
        mainViewModel.insertServer(edittext_server.text.toString(), edittext_ip.text.toString())
    }

    private fun onClickUpdateServer(){
        Log.d("Click", "Update")
        mainViewModel.update(edittext_server.text.toString(), edittext_ip.text.toString())
    }

    private fun onClickSearchServer(){
        var inputport :Int? = if (edittext_port.text.toString() == "")  0 else edittext_port.text.toString().toInt()
        mainViewModel.qureyServer(edittext_queryserver.text.toString(),
            edittext_protocal.text.toString(), inputport)
        Log.d("Click", "Query Server")
    }

    private fun onClickReadServer(){
        Log.d("Click", "Read")
        var result = mainViewModel.queryAll().observe(this, Observer {
            if (it?.size == 0) return@Observer
//            it?.apply {
//                Toast.makeText(this@MainActivity, "Read all server",Toast.LENGTH_SHORT).show()
//                for (server in it) {
//                    Log.d("Realm", "Server: $server")
//                }
//            }
        })
    }

    private fun onClickDeleteServer(){
        Log.d("Click", "Delete Server")
        mainViewModel.deleteServer()
    }

    private val result = Observer<RealmResults<Server?>?> {
        if(it == null) return@Observer

        println("Work This ${it.toString()}")
        for (server in it) {
            Log.d("Realm", "Read realm : \nserver: ${server?.name}")
            // txt_result.text = "${server?.getServerName()}  ${server?.getIP()}\n"
        }
    }


    fun run(view: View){
        mainViewModel.retrieveMember()
    }
}