package com.example.realmdb.modelServer

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Country {
    var id: Int = 0
    var name: String? = null
    var abbr: String? = null
    var updated: Date? = null
    var flag: String? = null
    var enable: Int = 0
}