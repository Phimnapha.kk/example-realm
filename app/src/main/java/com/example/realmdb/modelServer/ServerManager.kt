package com.example.realmdb.modelServer

import android.util.Log
import com.example.realmdb.api.ServerServicesResponse
import com.example.realmdb.models.Server
import com.example.realmdb.models.ServerMigration
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmQuery
import io.realm.RealmResults
import io.realm.kotlin.where
import org.json.JSONArray

class ServerManager {
    private var useRealm = Realm()
    private var port_id:Int =0

    fun insertServer(server : ServerList , port:Array<ServerPort>?){
        val portlist = port?.filter{item -> item.ip == server.ip}
        portlist?.forEach {
            port_id++
            it.id = port_id
            it.name = server.name
            server.port.add(it)
        }
        useRealm.insertOrUpdate(server)
        //useRealm.insert(server)
        /*realm?.executeTransaction {
            realm?.insert(server)
        }*/
    }



/*
    fun addServerToRealm(server : ServerList, id:Int){
        //realm = Realm.getInstance(config)
        realm?.executeTransaction {
            val user = realm?.createObject(ServerList::class.java,id)
        }
    }*/


    /*fun addServer(jsonserver: String?) {
        //try {

        val obj = JSONArray(jsonserver)

        for (i in 0 until obj.length()) {
            val item = obj.getJSONObject(i)
            item.put("id",i+1)
            Log.d("server","$item")
            //items.add(item)
        }

        val gson = Gson()
        val response = ServerServicesResponse()
        response.servers = gson.fromJson(jsonserver, Array<ServerList>::class.java)

       // realm = Realm.getInstance(config)
       // realmConfig.getInstanceConfig()
        realm?.beginTransaction()
        //add country to realm
        var maxId = realm?.where(ServerList::class.java)?.max("id")
        var newId: Int? = if (maxId == null) 1 else maxId.toInt() + 1
        var server = realm?.createAllFromJson(ServerList::class.java, obj)

        //var server = realm?.createObject(ServerList::class.java, newId)
        realm?.commitTransaction()
        Log.d("Realm", "Add Server")

        /*}catch (e : java.lang.Exception){
            Log.d("Realm Error ", "${e.message}")
        }*/
    }*/
}