package com.example.realmdb.modelServer

import android.util.Log
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmQuery
import io.realm.RealmResults
import io.realm.annotations.PrimaryKey
import io.realm.kotlin.deleteFromRealm

typealias Query<T> = RealmQuery<T>.() -> Unit
class Realm {
    private var realm: Realm? = null

    fun getRealm() : Realm?{
        realm = RealmConfig.getInstance()
        return realm
    }

    inline fun <reified T : RealmModel> deleteAll() {
        getRealm()?.apply {
            this.beginTransaction()
            this.delete(T::class.java)
            this.commitTransaction()
        }
    }

    inline fun <reified T : RealmModel> deletefromQuery(query: Query<T>) {
        getRealm()?.apply {
            this.beginTransaction()
            this.delete(T::class.java)
            this.commitTransaction()
        }
    }

    inline fun <reified T : RealmModel> forQuery() : RealmQuery<T>? {
        var query: RealmQuery<T>? = null
        getRealm()?.executeTransaction{
            query = it?.where(T::class.java)
        }
        return query
    }

    inline fun <reified T : RealmModel> queryAll() : RealmResults<T>? {
        var result: RealmResults<T>? = null
        getRealm()?.executeTransaction{
            result = it?.where(T::class.java)?.findAllAsync()
        }
        return result
    }

    inline fun <reified T : RealmModel> queryAllResultList() : List<T>? {
        var result = getRealm()?.where(T::class.java)?.findAll()
        return getRealm()?.copyFromRealm(result)
    }

    inline fun <reified T : RealmModel> queryFromField(fieldname: String, value: String) : RealmResults<T>? {
        Log.d("Query","Query $fieldname")
        var result:RealmResults<T>? = null
        getRealm()?.executeTransaction{
            result = it?.where(T::class.java)?.equalTo(fieldname,value)?.findAll()
        }
        return result
    }

    inline fun <reified T : RealmModel> queryContains(fieldname: String, value: String) : RealmResults<T>? {
        return getRealm()?.where(T::class.java)?.contains(fieldname,value)?.findAll()
    }

    inline fun <reified T : RealmModel> query(query: Query<T>) : RealmResults<T>? {
        return getRealm()?.where(T::class.java)?.findAll() //use query
    }

    inline fun <reified T : RealmModel> deletefromPrimary(fieldname: String, id: Int) {
        getRealm()?.apply {
            this.beginTransaction()
            this?.where(T::class.java)
                    ?.equalTo(fieldname, id)
                    ?.findFirst()
                    ?.deleteFromRealm()
            this?.commitTransaction()
        }
    }

     fun insert(model: RealmModel) {
         getRealm()?.executeTransaction{
            it?.insert(model)
        }
         Log.d("Insert","Insert")
    }

    fun insertOrUpdate(model: RealmModel) {
        getRealm()?.executeTransaction {
            it?.insertOrUpdate(model)
        }
        Log.d("Insert","InsertOrUpdate")
    }

    fun copyToRealm(model: RealmModel) {
        getRealm()?.executeTransaction{
            it?.copyToRealmOrUpdate(model)
        }
        Log.d("Insert","CopyToRealm")
    }

    fun clearEnableServer(){
        getRealm()?.executeTransaction {
            var result = it?.where(ServerList::class.java)?.findAll()
            result?.setInt("enable",0)
        }
    }

    fun clearUnavarible(value:Int? = 0){
        getRealm()?.executeTransaction {
            var result = it?.where(ServerList::class.java)?.equalTo("enable",value)?.findAll()
            for(i in result!!)
                Log.d("Delete",i.name.toString())
            result?.deleteAllFromRealm()
        }
    }

    inline fun <reified T : RealmModel> update(primaryKey: String,id: Int, name: String?,  model: RealmModel) {
        val value: T? = getRealm()?.where(T::class.java)
                ?.equalTo(primaryKey, id)
                ?.findFirst()
        getRealm()?.apply {
            this.beginTransaction()
            //set new value to model
            this.insertOrUpdate(model)
            this.commitTransaction()
        }
    }
}