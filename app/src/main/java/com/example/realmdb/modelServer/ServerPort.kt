package com.example.realmdb.modelServer

import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey

open class ServerPort : RealmObject() {
    @PrimaryKey
    var id:Int? = 0
    var name: String? = null
    var ip: String? = null
    var protocal: String? = null
    var port: Int? = 0
    var config_name: String? = null
    var enable: Int = 0
    @LinkingObjects("port")
    val owners: RealmResults<ServerList>? = null

    companion object{
        val TYPE_TCP = "tcp"
        val TYPE_UDP = "udp"
    }
}
