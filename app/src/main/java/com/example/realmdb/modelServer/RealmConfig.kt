package com.example.realmdb.modelServer

import android.content.Context
import com.example.realmdb.models.ServerMigration
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.exceptions.RealmMigrationNeededException
import java.io.File

class RealmConfig {
    companion object {
        private var realm: Realm? = null
        private var instance: Realm? = null
        private val config = RealmConfiguration.Builder()
                .name("realm.realm")
                .migration(ServerMigration())
                //.deleteRealmIfMigrationNeeded()
                .schemaVersion(5)
                .build()


        private fun setConfig() :Realm? {
            try{
                realm = Realm.getInstance(config)
                realm?.close()
            }catch (e: RealmMigrationNeededException){
                Realm.deleteRealm(config)
            }

            realm = Realm.getInstance(config)
            return  realm
        }

        fun getInstance(): Realm? {
            if (instance == null) {
               // realm = Realm.getInstance(config)
                instance = setConfig()
            }
            return instance
        }
    }
}