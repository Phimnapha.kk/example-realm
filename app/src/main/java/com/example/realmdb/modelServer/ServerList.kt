package com.example.realmdb.modelServer

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmModule
import java.util.*

open class ServerList : RealmObject() {
    @PrimaryKey
    var id: Int = 0
    var name: String? = null
    var ip: String? = null

    var updated: Date? = null //add
    var updated_2: String? = null //add
    var sort: Int = 0
    var country: String? = null
    var domain: String? = null
    var user_type: String? = null
    var enable: Int = 0
    var pingValue: Float = 0f
    var port: RealmList<ServerPort> = RealmList()
}