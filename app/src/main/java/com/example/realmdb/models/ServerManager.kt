package com.example.realmdb.models

import android.util.Log
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmMigration
import io.realm.RealmResults
import java.util.*

public class ServerManager {
    private var realm: Realm? = null
    private var config = RealmConfiguration.Builder()
        .name("realm.realm")
        //.migration(ServerMigration())
        .build()

    fun getServer(): RealmResults<Server?>? {
        realm = Realm.getInstance(config)
        return realm?.where(Server::class.java)?.findAll()
    }

    fun addServer(server: Server?) {
        realm?.beginTransaction()
        realm?.insert(server)
        realm?.commitTransaction()
    }

    fun addServerfromCreate(name: String, ip:String) {

        //try {
            realm = Realm.getInstance(config)
            realm?.beginTransaction()
            var maxId = realm?.where(Server::class.java)?.max("id")
            var newId: Int? = if (maxId == null) 1 else maxId.toInt() + 1
            Log.d("Realm", "Before add $newId $name $ip")
            var server = realm?.createObject(Server::class.java, newId)
            //server?.setID(newId)
            server?.setServerName(name)
            server?.setIP(ip)
            realm?.commitTransaction()
            Log.d("Realm", "Add")
        /*}catch (e : java.lang.Exception){
            Log.d("Realm Error ", "${e.message}")
        }*/
    }

    fun deleteServer(id: Int) {
        realm?.beginTransaction()
        realm?.where(Server::class.java)
                ?.equalTo("id", id)
                ?.findFirst()
                ?.deleteFromRealm()
        realm?.commitTransaction()
    }

    fun clearAll() {
        realm?.beginTransaction()
        realm?.delete(Server::class.java)
        realm?.commitTransaction()
    }
}