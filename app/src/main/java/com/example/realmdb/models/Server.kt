package com.example.realmdb.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Server : RealmObject() {
    @PrimaryKey
    var  id : Int? = 0
   //var id = UUID.randomUUID().toString()
    lateinit var name : String
    lateinit var ip : String

    fun setID(id : Int?) {
        this.id = id
    }

    fun getID(): Int? {
        return this.id
    }

    fun setServerName(name: String) {
        this.name = name
    }
    fun getServerName(): String? {
        return this.name
    }

    fun setIP(ip: String) {
        this.ip = ip
    }
    fun getIP(): String? {
        return this.ip
    }
}