package com.example.realmdb.models

import android.util.Log
import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.modelServer.ServerPort
import io.realm.DynamicRealm
import io.realm.DynamicRealmObject
import io.realm.FieldAttribute
import io.realm.RealmMigration
import io.realm.annotations.RealmModule
import java.util.*


class ServerMigration : RealmMigration {
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var oldVersion = oldVersion
        Log.d("Oldversion","$oldVersion")
        Log.d("Newversion","$newVersion")
        val schema = realm.schema
        /*if (oldVersion == 1L) {
            schema.get("Server")!!
                .addField("id", Int::class.javaObjectType, FieldAttribute.PRIMARY_KEY)
                //.removeField("id")
                //.addField("id", Int::class.javaPrimitiveType, FieldAttribute.PRIMARY_KEY)

                   schema.create("ServerList")
                  .addField("id", Int::class.java, FieldAttribute.PRIMARY_KEY)
                  .addField("name", String::class.java)
                  .addField("ip", String::class.java)
                  .addField("sort", Int::class.java)
                  .addField("country", String::class.java)
                  .addField("domain", String::class.java)
                  .addField("user_type", String::class.java)
            oldVersion++
        }*/

        if (oldVersion == 1L) {
            Log.d("Migrate","0_1")
            schema.get("ServerList")!!
                .addField("updated", Date::class.java)

            oldVersion++
        }

        if (oldVersion == 2L) {
            Log.d("Migrate","1_2")
            schema.get("ServerList")!!
                .removeField("updated")
            oldVersion++
        }

        if (oldVersion == 3L) {
            Log.d("Migrate","2_3")
            schema.get("ServerList")!!
                    .addField("updated", Date::class.java, FieldAttribute.REQUIRED)
                    .transform { obj: DynamicRealmObject->
                        obj.setDate("updated", Date())
                    }
            oldVersion++
        }

        if (oldVersion == 4L) {
            Log.d("Migrate","3_4")
            schema.get("ServerList")!!
                    .addField("updated_2", String::class.java, FieldAttribute.REQUIRED)
                    //.setRequired("updated_2", true)
            oldVersion++
        }


        /*
        if(oldVersion == 1L)
        {
            Log.d("Migrate","1_2")
            schema.create("Domain")
                    .addField("id", Int::class.java, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String::class.java)
                    .addField("url", String::class.java)
                    .addField("enable", Int::class.java)
            oldVersion++
        }*/
    }
}