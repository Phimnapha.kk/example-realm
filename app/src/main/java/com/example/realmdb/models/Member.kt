package com.example.realmdb.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Member {
    @PrimaryKey
    var studentId = 0
    var firstName: String? = null
    var lastName: String? = null
    var age = 0
    var gender: String? = null
    var city: String? = null //Create getter and setter

    /* open fun Member() {
         this.studentId = 0
         this.firstName = ""
     }*/

    fun setfirstName(name: String?) {
        this.firstName = name
    }
    fun getfirstName(): String? {
        return this.firstName
    }

    fun setID(id: Int) {
        this.studentId = id
    }
    fun getID(): Int? {
        return this.studentId
    }
}
