package com.example.realmdb.api

import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.modelServer.ServerPort
import com.example.realmdb.models.Server

class ServerServicesResponse {
    //var countries: Array<Country>? = null
    var servers: Array<ServerList>? = null
    var serverPorts: Array<ServerPort>? = null
   /* var config: String? = null
    var private: ArrayList<PrivateServerResponse>? = null
    var proxy: Array<Proxy>? = null
    var premium: Boolean? = null*/
}