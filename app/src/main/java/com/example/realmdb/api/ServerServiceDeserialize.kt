package com.example.realmdb.api


import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.modelServer.ServerPort
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParseException

import java.lang.reflect.Type
import java.util.*
import kotlin.collections.HashMap

class ServerServiceDeserialize : JsonDeserializer<ServerServicesResponse> {
    private val classTag = "ServerServiceDeserialize"
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ServerServicesResponse {
        val response = ServerServicesResponse()
        val gson = Gson()
        val jsonObject = json.asJsonObject
        //response.countries = gson.fromJson(jsonObject.get("countries"), Array<Country>::class.java)
        response.servers = gson.fromJson(jsonObject.get("servers"), Array<ServerList>::class.java)
        val serverPorts = ArrayList<ServerPort>()
        val portsObject = jsonObject.getAsJsonObject("ports")
        convertServerPort(portsObject.getAsJsonObject("tcp"), ServerPort.TYPE_TCP, serverPorts)
        convertServerPort(portsObject.getAsJsonObject("udp"), ServerPort.TYPE_UDP, serverPorts)
        response.serverPorts = serverPorts.toTypedArray()
       /* response.config = gson.fromJson(jsonObject.get("config"), String::class.java)
        response.private = parsePrivateServer(jsonObject.getAsJsonArray("private"))
        response.proxy = gson.fromJson(jsonObject.get("proxy"), Array<Proxy>::class.java)
        response.premium = gson.fromJson(jsonObject.get("premium"), Boolean::class.java)*/
        return response
    }

    private fun convertServerPort(ports: JsonObject, type: String, portList: ArrayList<ServerPort>) {
        val portKey = ports.keySet()
        for (key in portKey) {
            val portInt = Integer.parseInt(key)
            val ipArray = ports.getAsJsonArray(key)
            var index = 0
            val size = ipArray.size()
            while (index < size) {
                val jsonElement = ipArray.get(index)
                val ip = jsonElement.asString

                val serverPort = ServerPort()
                serverPort.ip = ip
                serverPort.protocal = type
                serverPort.port = portInt
                serverPort.config_name = null
                //serverPort.updated = null
                portList.add(serverPort)
                ++index
            }
        }
    }
}
