package com.example.realmdb.api

import android.content.Context
import android.util.Log
import com.example.realmdb.modelServer.Realm
import com.example.realmdb.modelServer.ServerList
import com.example.realmdb.modelServer.ServerManager
import com.example.realmdb.modelServer.ServerPort
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import java.io.InputStream


class ServerRepository {
    private  var serverDb = ServerManager()

    fun doRequest(context: Context) : Boolean?{

        var inpustream: InputStream = context.assets.open("serverlist.json")
        var jsonfile : String = inpustream.bufferedReader().use { it.readText() }
        //after request api get response
        parseAndUpdateData(jsonfile)
        return true
        //Log.d("Json file","$jsonfile")
        //ServerManager().addServer(jsonfile)
    }

    private fun parseAndUpdateData(responsebody : String) {
        val gson = GsonBuilder().registerTypeAdapter(ServerServicesResponse::class.java, ServerServiceDeserialize()).create()
            ?: return
        val serverServicesResponse: ServerServicesResponse
        try {
            serverServicesResponse = gson.fromJson(responsebody, ServerServicesResponse::class.java) ?: throw Exception()
        } catch (e: Exception) {
            Log.d("parse", "failed to parse response json")
            Log.d("parse", e.message!!)
            return
        }
        val servers = serverServicesResponse.servers
        val server_port = serverServicesResponse.serverPorts

        //update enable
        Realm().clearEnableServer()
        //if(Realm().queryAll<ServerList>()?.size != 0) return
        servers?.let {
            addListServer(it, server_port)
        }
        Realm().clearUnavarible()
    }

    private fun addListServer(serverList : Array<ServerList> , port:Array<ServerPort>?){
        var result = Realm().forQuery<ServerList>()?.findAll()
            var i:Int = 0
            serverList.forEach {server->
                i++
                server.id = i
                server.enable = 1
                //find id(primary key) from domain
                /*result?.find { item -> item.domain == server.domain }?.apply {
                    server.id = this.id
                        Log.d("Update from id","$result.id")
                }*/

                serverDb.insertServer(server , port)

                Log.d("Add or update",server.name!!)
        }
    }
}